package sbu.cs;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FunctionTesting {

    private static PinkBlock functions;

    @BeforeAll
    static void setup() {
        //I guess A better approach would have NOT needed any arguments to be passed to constructor:
        functions = new PinkBlock(1,"mostafa","abdolmaleki");
    }

    @Test
    void blackFunction1 (){
        assertEquals(functions.function1("abdolmaleki"),"ikelamlodba");
    }

    @Test
    void blackFunction2 (){
        assertEquals(functions.function2("abdolmaleki"),"aabbddoollmmaalleekkii");
    }

    @Test
    void blackFunction3 (){
        assertEquals(functions.function3("abdolmaleki"),"abdolmalekiabdolmaleki");
    }

    @Test
    void blackFunction4 (){
        assertEquals(functions.function4("abdolmaleki"),"iabdolmalek");
    }

    @Test
    void blackFunction5 (){
        assertEquals(functions.function5("abdolmaleki"),"zywlonzovpr");
    }

    @Test
    void whiteFunction1(){
        //Two asserts to test if the function is revisable:
        assertEquals(functions.function1("mostafa","abdolmaleki"),"maobsdtoalfmaaleki");
        assertEquals(functions.function1("abdolmaleki","mostafa"),"ambodsotlamfaaleki");
    }

    @Test
    void whiteFunction2 (){
        //Two asserts to test if the function is revisable:
        assertEquals(functions.function2("mostafa","abdolmaleki"),"mostafaikelamlodba");
        assertEquals(functions.function2("abdolmaleki","mostafa"),"abdolmalekiafatsom");
    }

    @Test
    void whiteFunction3 (){
        //Two asserts to test if the function is revisable:
        assertEquals(functions.function3("mostafa","abdolmaleki"),"mioksetlaafmalodba");
        assertEquals(functions.function3("abdolmaleki","mostafa"),"aabfdaotlsmoamleki");
    }

    @Test
    void whiteFunction4 (){
        //Two asserts to test if the function is revisable:
        assertEquals(functions.function4("mostafa","abdolmaleki"),"abdolmaleki");
        assertEquals(functions.function4("abdolmaleki","mostafa"),"mostafa");
    }

    @Test
    void whiteFunction5 (){
        //Two asserts to test if the function is revisable:
        assertEquals(functions.function5("mostafa","abdolmaleki"),"mpvhlraleki");
        assertEquals(functions.function5("abdolmaleki","mostafa"),"mpvhlraleki");
    }
}