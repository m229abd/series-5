package sbu.cs;

public class Block {
    protected String horizontalOutput;
    protected String verticalOutput;
    //Black Function 1:
    public String function1 (String input) {
        StringBuilder  ans = new StringBuilder(input);
        return ans.reverse().toString();
    }
    //Black Function 2:
    public String function2 (String input) {
        StringBuilder ans = new StringBuilder();
        for (char ch: input.toCharArray())
            ans.append(ch).append(ch);
        return ans.toString();
    }
    //Black Function 3:
    public String function3 (String input) {
        return input + input;
    }
    //Black Function 4:
    public String function4 (String input) {
        return input.charAt(input.length() - 1) + input.substring(0,input.length() - 1);
    }
    //Black Function 5:
    public String function5 (String input) {
        StringBuilder ans = new StringBuilder();
        for (char ch: input.toCharArray()){
            //Find the mirror char:
            char reverse = (char)(122 - ((int)(ch) - 97));
            ans.append(reverse);
        }
        return ans.toString();
    }
    public String getHorizontalOutput(){
        return horizontalOutput;
    }
    public String getVerticalOutput(){
        return verticalOutput;
    }
}
