package sbu.cs;

public class PinkBlock extends Block {
    public PinkBlock(int option, String verticalInput, String horizontalInput){
        super();
        //Take the output and apply funtions:
        switch(option){
            case 1:
                horizontalOutput = function1(horizontalInput,verticalInput);
                break;
            case 2:
                horizontalOutput = function2(horizontalInput,verticalInput);
                break;
            case 3:
                horizontalOutput = function3(horizontalInput,verticalInput);
                break;
            case 4:
                horizontalOutput = function4(horizontalInput,verticalInput);
                break;
            case 5:
                horizontalOutput = function5(horizontalInput,verticalInput);
        }
        verticalOutput = horizontalOutput;
    }
    //White Function 1:
    public String function1 (String input1, String input2) {
        StringBuilder ans = new StringBuilder();
        int i = 0, j = 0;
        while(i < input1.length() || j < input2.length()){
            if(i < input1.length())
                ans.append(input1.charAt(i++));
            if(j < input2.length())
                ans.append(input2.charAt(j++));
        }
        return ans.toString();
    }
    //White Function 2:
    public String function2 (String input1, String input2) {
        return input1 + super.function1(input2);
    }
    //White Function 3:
    public String function3 (String input1, String input2) {
        return this.function1(input1, super.function1(input2));
    }
    //White Function 4:
    public String function4 (String input1, String input2) {
        if (input1.length() % 2 == 0) 
            return input1;
        return input2;
    }
    //White Function 5:
    public String function5 (String input1, String input2) {
        StringBuilder ans = new StringBuilder();
        int minCount = Math.min(input1.length(),input2.length());
        //Does the given process:
        for (int i = 0; i < minCount; i++)
            ans.append((char)(((((int)(input1.charAt(i) - 96)) + ((int)(input2.charAt(i) - 96))) % 26) + 95));
        if (minCount == input1.length())
            ans.append(input2.substring(minCount));
        if (minCount == input2.length()) 
            ans.append(input1.substring(minCount));
        /* becaues the mod function return 0 and 1 and the should be (25 + 0 = y) and (25 + 1 = z)
        but there is no loop in ascii so we should replace them*/
        String asciiFix = ans.toString().replace("`", "z").replace("_", "y");
        return asciiFix;
    }
}
