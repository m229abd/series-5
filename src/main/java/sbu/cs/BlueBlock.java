package sbu.cs;

public class BlueBlock extends Block {
    public BlueBlock(int option, String verticalInput, String horizontalInput){
        super();
        //Take the output and apply funtions:
        switch(option){
            case 1:
                horizontalOutput = function1(horizontalInput);
                verticalOutput = function1(verticalInput);
                break;
            case 2:
                horizontalOutput = function2(horizontalInput);
                verticalOutput = function2(verticalInput);
                break;
            case 3:
                horizontalOutput = function3(horizontalInput);
                verticalOutput = function3(verticalInput);
                break;
            case 4:
                horizontalOutput = function4(horizontalInput);
                verticalOutput = function4(verticalInput);
                break;
            case 5:
                horizontalOutput = function5(horizontalInput);
                verticalOutput = function5(verticalInput);
        }
    }
}
