package sbu.cs;

import java.util.Arrays;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
        for(int i = 0; i < size - 1; i++){
            int small = i;
            //j = min(arr[i:n])
            for(int j = i; j < size; j++)
                if(arr[j] < arr[small])
                    small = j;
            //Swap(i,j):
            int temp = arr[i];
            arr[i] = arr[small];
            arr[small] = temp;
        }
        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        for(int i = 1; i < size; i++){
            int current = arr[i];
            int small = i - 1;
            //If current < arr[small], shift the element and check the next one:
            for(; small >= 0 && current < arr[small]; small--)
                arr[small + 1] = arr[small];
            //When reached a biggr number (or the beginning), insert the element:
            arr[small + 1] = current;
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        int out[] = new int[size];
        if(size > 2){
            //if the array is bigger then 2, split the arr into two halves
            int s1 = size/2;
            int s2 = size - s1;
            int a1[] = new int[s1];
            int a2[] = new int[s2];
            for(int i = 0; i < s1; i++)
                a1[i] = arr[i];
            for(int j  = 0; j < s2; j++)
                a2[j] = arr[s1 + j];
            //Sort each half:
            a1 = mergeSort(a1, s1);
            a2 = mergeSort(a2, s2);
            //Merge two halves:
            out = merge(a1,a2);
            return out;
        }
        //If the array is only one element, sorting doesn't mean anything:
        else if(size == 1)
            return arr;
        //If the array is 2 elements and sorted, no need to do anything:
        else if (arr[0] < arr[1])
            return arr;
        //If the array is 2 elements but not sorted, sort it:
        out[0] = arr[1];
        out[1] = arr[0];
        return out;
    }
    public int[] merge(int[] arr1, int[] arr2) {
        int i1 = 0, s1 = arr1.length;
        int i2 = 0, s2 = arr2.length;
        int[] out = new int[s1 + s2];
        while(i1 < s1 && i2 < s2)
            if(arr1[i1] < arr2[i2])
                out[i1 + i2] = arr1[i1++];
            else
                out[i1 + i2] = arr2[i2++];
        while(i1 < s1)
            out[i1 + i2] = arr1[i1++];
        while(i2 < s2)
            out[i1 + i2] = arr2[i2++];
        return out;
    }


    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int sIndex = 0;
        int eIndex = arr.length - 1;
        int mid = 0;
        while(eIndex - sIndex > 1){
            //Devide arrays to two halves:
            mid = sIndex + ((eIndex - sIndex)/2);
            //Compare the value to each half:
            if(arr[mid] == value)
                return mid;
            else if(arr[mid] < value)
                sIndex = mid;
            else
                eIndex = mid;
        }
        //Ckeck the last element:
        if(arr[mid] == value)
            return mid;
        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        //Devide arrays to two halves:
        int mid = arr.length/2;
        //Compare the value to each half:
        if(arr[mid] == value)
            return mid;
        //Repeat this process:
        else if(arr.length > 1)
            if(arr[mid] < value){
                int ans = binarySearchRecursive(Arrays.copyOfRange(arr, mid, arr.length),value);
                if(ans == -1)
                    return -1;
                return mid + ans;
            }
            else
                return binarySearchRecursive(Arrays.copyOfRange(arr, 0, mid),value);
        return -1;
    }
}
