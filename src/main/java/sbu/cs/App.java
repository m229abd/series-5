package sbu.cs;

public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input) {
        Block machine[][] = new Block[n][n];
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                //Initilizing first Block:
                if (i == 0 && j == 0)
                    machine[i][j] = new GreenBlock(arr[i][j], input);
                //Initilize GreenBlocks:
                else if(i == 0 && j != n - 1)
                    machine[i][j] = new GreenBlock(arr[i][j], machine[i][j - 1].getHorizontalOutput());
                else if(j == 0 && i != n - 1)
                    machine[i][j] = new GreenBlock(arr[i][j], machine[i - 1][j].getVerticalOutput());
                //Initilize YellowBlocks:
                else if(i == 0)
                    machine[i][j] = new YellowBlock(arr[i][j], machine[i][j - 1].getHorizontalOutput());
                else if(j == 0)
                    machine[i][j] = new YellowBlock(arr[i][j], machine[i - 1][j].getVerticalOutput());
                //Initializing Blue:
                else if(i > 0 && i < n - 1 && j > 0 && j < n - 1)
                    machine[i][j] = new BlueBlock(arr[i][j], machine[i - 1][j].getVerticalOutput(), machine[i][j - 1].getHorizontalOutput());
                //Initializing PinkBlocks:
                else if(i == n - 1)
                    machine[i][j] = new PinkBlock(arr[i][j], machine[i - 1][j].getVerticalOutput(), machine[i][j - 1].getHorizontalOutput());
                else if(j == n - 1)
                    machine[i][j] = new PinkBlock(arr[i][j], machine[i][j - 1].getHorizontalOutput(), machine[i - 1][j].getVerticalOutput());
            }
        }
        //Take the output from the last block:
        return machine[n - 1][n - 1].getVerticalOutput();
    }
}
