package sbu.cs;

public class YellowBlock extends Block {
    //Take the output and apply funtions:
    public YellowBlock(int option, String input){
        super();
        switch(option){
            case 1:
                horizontalOutput = function1(input);
                break;
            case 2:
                horizontalOutput = function2(input);
                break;
            case 3:
                horizontalOutput = function3(input);
                break;
            case 4:
                horizontalOutput = function4(input);
                break;
            case 5:
                horizontalOutput = function5(input);
        }
        //Same output from each side:
        verticalOutput = horizontalOutput;
    }
}
